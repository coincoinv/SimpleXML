package moonstone;

import moonstone.xml.XML;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.List;

@XML.XmlRootElement(name = "root")
public record RecordXmlTest(@XML.XmlAttribute(name = "num")
                            Integer num,
                            @XML.XmlAttribute(name = "name")
                            String name,
                            @XML.XmlElement(name = "content", type = Content.class)
                            List<Content> content) {

    public static record Content(
            @XML.XmlAttribute(name = "version")
            String version,
            @XML.XmlElement(name = "data", type = BigDecimal.class)
            BigDecimal data) {

    }

    public static class TestMe {
        @Test
        public void parse() throws Exception {
            XML xml = XML.parse(new ByteArrayInputStream(("<root num='2048' name='joke'>"
                    + "<content version='V1'><data>4.58</data></content><content version='V2'/>"
                    + " </root>").getBytes()));
            RecordXmlTest recordXmlTest = xml.toJava(RecordXmlTest.class);
            assert (recordXmlTest.name().equals("joke"));
            assert (recordXmlTest.num == 2048);
            assert recordXmlTest.content().get(0).version.equals("V1");
            assert recordXmlTest.content().get(0).data.toString().equals("4.58");
            assert recordXmlTest.content().get(1).data == null;
            System.out.println(xml);
        }
    }
}
