package moonstone;

import moonstone.xml.XML;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

@XML.XmlRootElement(name = "app")
public class Pojo {
    @XML.XmlElement(name = "data", type = String.class)
    String data;
    @XML.XmlAttribute(name = "me")
    Boolean me;
    @XML.XmlElement(name = "app", type = Pojo.class)
    Pojo pojo;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getMe() {
        return me;
    }

    public void setMe(Boolean me) {
        this.me = me;
    }

    public Pojo getPojo() {
        return pojo;
    }

    public void setPojo(Pojo pojo) {
        this.pojo = pojo;
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "data='" + data + '\'' +
                ", me=" + me +
                ", pojo=" + pojo +
                '}';
    }

    @Test
    public void pojoTest() throws Exception {
        Pojo pojo = XML.parse(new ByteArrayInputStream("<app me='true'><data>one</data><app me='false'/></app>".getBytes(StandardCharsets.UTF_8))).toJava(Pojo.class);
        System.out.println(pojo);
        assert pojo.getMe();
        assert pojo.getData().equals("one");
        assert !pojo.getPojo().getMe();
        assert null == pojo.getPojo().getPojo();
    }
}
