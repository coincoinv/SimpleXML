package moonstone.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface XML {

    String name();

    String path();

    List<XML> content();


    Set<String> elementNameSet();

    Iterator<XML> elementByName(String name);

    Map<String, String> attributes();

    default String version() {
        return "1.0";
    }

    default String encoding() {
        return "UTF-8";
    }

    default String toString(boolean pretty) {
        return toString();
    }

    Type type();

    enum Type {
        Quote, String, Element, Boolean, Number, Meta
    }

    default <T> T toJava(Class<T> beanWithXMLDefinition) throws Exception {
        // skip the root if possible
        Annotation rootElement = Context.getAnnotation(beanWithXMLDefinition, Context.xmlRootElement());
        String root = rootElement == null ? null : Context.name(rootElement);
        XML node = this;
        if (root != null && !root.equals(node.name())) {
            node = node.elementByName(root).next();
        }
        Object primitiveJava = node.content().isEmpty() ? null : Context.toPrimitiveJava(beanWithXMLDefinition, node.content().get(0).toString());
        if (primitiveJava != null) {
            return (T) primitiveJava;
        }
        Constructor<?> pojo = null;
        Constructor<?> max = null;
        for (Constructor<?> constructor : beanWithXMLDefinition.getConstructors()) {
            // get max one or the zero one
            if (beanWithXMLDefinition.isRecord()) {
                max = beanWithXMLDefinition.getConstructors()[0];
                break;
            }
            if (constructor.getParameterCount() == 0) {
                pojo = constructor;
            } else {
                if (max == null) {
                    max = constructor;
                } else {
                    max = constructor.getParameterCount() > max.getParameterCount() ? constructor : max;
                }
            }
        }
        Map<String, Object> fields = new LinkedHashMap<>();
        Map<String, Method> methodMap = new LinkedHashMap<>();
        Set<String> fieldNameSet = new HashSet<>();
        for (Field field : beanWithXMLDefinition.getDeclaredFields()) {
            fieldNameSet.add(field.getName());
        }
        for (Method declaredMethod : beanWithXMLDefinition.getDeclaredMethods()) {
            if (!beanWithXMLDefinition.isRecord()) {
                if ((declaredMethod.getParameterCount() == 0 && declaredMethod.getReturnType() == Void.class) || declaredMethod.getParameterCount() > 1) {
                    continue;
                }
            }
            Annotation xmlElement = Context.getAnnotation(declaredMethod, Context.xmlElement());
            Annotation attribute = Context.getAnnotation(declaredMethod, Context.xmlAttribute());
            String fieldNameMaybe;
            if (beanWithXMLDefinition.isRecord()) {
                fieldNameMaybe = declaredMethod.getName();
            } else {
                fieldNameMaybe = declaredMethod.getName().length() < 4 ? "" : declaredMethod.getName().substring(3, 4).toLowerCase(Locale.ROOT) + declaredMethod.getName().substring(4);
            }
            if (xmlElement == null && attribute == null && fieldNameSet.contains(fieldNameMaybe)) {
                xmlElement = Context.getAnnotation(beanWithXMLDefinition.getDeclaredField(fieldNameMaybe), Context.xmlElement());
                attribute = Context.getAnnotation(beanWithXMLDefinition.getDeclaredField(fieldNameMaybe), Context.xmlAttribute());
            }
            String name = fieldNameMaybe;
            if (attribute != null) {
                String namespace = (Context.namespace(attribute).isEmpty() || "##default".equals(Context.namespace(attribute)))
                        ? "" : Context.namespace(attribute) + ":";
                name = (!"##default".equals(Context.name(attribute)) && !Context.name(attribute).isEmpty())
                        ? Context.name(attribute) : name;
                name = namespace + name;
            }
            if (!beanWithXMLDefinition.isRecord()) {
                if (attribute == null && name.startsWith("set")) {
                    name = name.substring(3, 4).toLowerCase(Locale.ROOT) + name.substring(4);
                }
                // the pojo will only use the pojo name
                if (!declaredMethod.getName().startsWith("get") && !declaredMethod.getName().startsWith("set")) {
                    continue;
                }
            } else {
                // the record will only use the field name
                if (!fieldNameSet.contains(declaredMethod.getName())) {
                    continue;
                }
            }
            boolean attMatch = node.attributes().get(declaredMethod.getName()) != null;
            if (declaredMethod.getName().startsWith("set")) {
                methodMap.put(name, declaredMethod);
            }
            if (attribute != null || attMatch) {
                String value = node.attributes().get(name);
                if (value == null && (attribute != null && Context.required(attribute))) {
                    throw new RuntimeException("Attribute " + name + " Lost, but " + beanWithXMLDefinition.getName() + " require the Attribute at" + this);
                }
                Class<?> type = declaredMethod.getParameterCount() == 0 ? declaredMethod.getReturnType() : declaredMethod.getParameterTypes()[0];
                Annotation adapter = Context.getAnnotation(declaredMethod, Context.xmlAdapter());
                if (adapter == null) {
                    adapter = Context.getAnnotation(type, Context.xmlAdapter());
                }
                Object arg;
                if (adapter == null) {
                    arg = null;
                    if (value == null) {
                        fields.put(name, arg);
                        continue;
                    }
                    arg = Context.toPrimitiveJava(type, value);
                } else {
                    arg = Context.adapter(adapter, value);
                }
                fields.put(name, arg);
                // now let's work with this
                continue;
            }
            name = xmlElement == null ? name : Context.name(xmlElement);
            List<Object> beans = new LinkedList<>();
            Class<?> fieldType = declaredMethod.getParameterCount() > 0 ? declaredMethod.getParameterTypes()[0] : declaredMethod.getReturnType();
            Class<?> beanType = fieldType;
            if (xmlElement != null) {
                beanType = Context.type(xmlElement);
            }
            if (beanType.isAssignableFrom(Collection.class)) {
                Annotation adapter = Context.getAnnotation(declaredMethod, Context.xmlAdapter());
                if (xmlElement == null && adapter == null) {
                    throw new RuntimeException("can't understand the type, give a adapter or type define please");
                }
                if (adapter == null) {
                    beanType = Context.type(xmlElement);
                }
            }
            if (!node.elementByName(name).hasNext())
                continue;
            Iterator<XML> iterator = node.elementByName(name);
            for (XML xml = iterator.next(); ; xml = iterator.next()) {
                Annotation adapter = Context.getAnnotation(declaredMethod, Context.xmlAdapter());
                if (adapter == null) {
                    adapter = Context.getAnnotation(beanType, Context.xmlAdapter());
                }
                if (adapter != null) {
                    beans.add(Context.adapter(adapter, xml));
                } else {
                    beans.add(xml.toJava(beanType));
                }
                if (!iterator.hasNext()) {
                    break;
                }
            }
            if (Collection.class.isAssignableFrom(fieldType)) {
                Object arg = beans;
                if (Set.class.isAssignableFrom(fieldType)) {
                    arg = new HashSet<>(beans);
                }
                if (Map.class.isAssignableFrom(fieldType)) {
                    var map = new LinkedHashMap<>();
                    var i = 0;
                    Iterator<XML> xmlIterator = node.elementByName(name);
                    for (XML xml = xmlIterator.next(); ; xml = xmlIterator.next()) {
                        map.put(Optional.ofNullable(xml.attributes().get("key")).orElse(i + ""), beans.get(i++));
                        if (!xmlIterator.hasNext()) {
                            break;
                        }
                    }
                    arg = map;
                }
                fields.put(name, arg);
            } else {
                fields.put(name, beans.get(0));
            }
        }
        if (max == null && pojo != null) {
            T bean = (T) pojo.newInstance();
            for (Map.Entry<String, Method> pair : methodMap.entrySet()) {
                Annotation attribute = Context.getAnnotation(pair.getValue(), Context.xmlAttribute());
                Annotation element = Context.getAnnotation(pair.getValue(), Context.xmlElement());
                // if the is setter use the field-name
                String fieldNameMaybe = pair.getKey();
                if (element == null && attribute == null && fieldNameSet.contains(fieldNameMaybe)) {
                    element = Context.getAnnotation(beanWithXMLDefinition.getDeclaredField(fieldNameMaybe), Context.xmlElement());
                    attribute = Context.getAnnotation(beanWithXMLDefinition.getDeclaredField(fieldNameMaybe), Context.xmlAttribute());
                }
                String name = pair.getKey();
                if (attribute != null) {
                    String namespace = (Context.namespace(attribute).isEmpty() || "##default".equals(Context.namespace(attribute)))
                            ? "" : Context.namespace(attribute) + ":";
                    name = (!"##default".equals(Context.name(attribute)) && !Context.name(attribute).isEmpty())
                            ? Context.name(attribute) : name;
                    name = namespace + name;
                }
                if (element != null) {
                    name = Context.name(element);
                }
                pair.getValue().invoke(bean, fields.get(name));
            }
            return bean;
        } else if (max != null) {
            // only read from the method for security
            List<Object> args = new ArrayList<>(max.getParameterCount());
            for (Parameter parameter : max.getParameters()) {
                String name = parameter.getName();
                Annotation xmlElement = Context.getAnnotation(parameter, Context.xmlElement());
                Annotation xmlAttribute = Context.getAnnotation(parameter, Context.xmlAttribute());
                if (xmlElement != null && !"##default".equals(Context.name(xmlElement))) {
                    name = Context.name(xmlElement);
                }
                if (xmlAttribute != null) {
                    if (!"##default".equals(Context.name(xmlAttribute))) {
                        name = Context.name(xmlAttribute);
                    }
                    String ns = Context.namespace(xmlAttribute);
                    if (!"##default".equals(ns) && !ns.isEmpty()) {
                        name = Context.namespace(xmlAttribute) + ":" + name;
                    }
                }
                args.add(fields.get(name));
            }
            return (T) max.newInstance(args.toArray());
        }
        throw new RuntimeException("NONE CONSTRUCTOR!!!");
    }

    /**
     * describe the parse now status
     **/
    enum XMLType {
        Quote, Attribute, Element, CDATA
    }

    enum Context {
        ;

        public static String name(Object o) {
            try {
                return (String) o.getClass().getMethod("name")
                        .invoke(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Class<?> valueType(Annotation o) {
            try {
                return (Class<?>) o.getClass().getMethod("value").invoke(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static String namespace(Object o) {
            try {
                return (String) o.getClass().getMethod("namespace")
                        .invoke(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Boolean required(Object o) {
            try {
                return (Boolean) o.getClass().getMethod("required")
                        .invoke(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Object unmarshal(Object o, Object xml) {
            try {
                return Stream.of(o.getClass().getMethods()).filter(m -> m.getName().equals("unmarshal"))
                        .findFirst().orElseThrow(() -> new RuntimeException("unmarshal Method not found")).invoke(o, xml);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Class<?> type(Object o) {
            try {
                return (Class<?>) o.getClass().getMethod("type")
                        .invoke(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Annotation getAnnotation(Parameter bean, String annotationName) {
            for (Annotation annotation : bean.getAnnotations()) {
                if (annotation.annotationType().getName().endsWith(annotationName)) {
                    return annotation;
                }
            }
            return null;
        }

        public static Annotation getAnnotation(Class<?> bean, String annotationName) {
            for (Annotation annotation : bean.getAnnotations()) {
                if (annotation.annotationType().getName().endsWith(annotationName)) {
                    return annotation;
                }
            }
            return null;
        }

        public static Annotation getAnnotation(Field bean, String annotationName) {
            for (Annotation annotation : bean.getAnnotations()) {
                if (annotation.annotationType().getName().endsWith(annotationName)) {
                    return annotation;
                }
            }
            return null;
        }

        public static Annotation getAnnotation(Method bean, String annotationName) {
            for (Annotation annotation : bean.getAnnotations()) {
                if (annotation.annotationType().getName().endsWith(annotationName)) {
                    return annotation;
                }
            }
            return null;
        }

        public static String xmlAdapter() {
            return "XmlJavaTypeAdapter";
        }

        public static String xmlRootElement() {
            return "XmlRootElement";
        }

        public static String xmlAttribute() {
            return "XmlAttribute";
        }

        public static String xmlElement() {
            return "XmlElement";
        }

        public static Object toPrimitiveJava(Class<?> type, Object value) {
            Object arg = null;
            if (String.class.isAssignableFrom(type)) {
                arg = value;
            }
            if (Boolean.class.isAssignableFrom(type)) {
                arg = Boolean.parseBoolean(value.toString());
            }
            if (Long.class.isAssignableFrom(type)) {
                arg = Long.parseLong(value.toString());
            }
            if (Integer.class.isAssignableFrom(type)) {
                arg = Integer.parseInt(value.toString());
            }
            if (Double.class.isAssignableFrom(type)) {
                arg = Double.parseDouble(value.toString());
            }
            if (Float.class.isAssignableFrom(type)) {
                arg = Float.parseFloat(value.toString());
            }
            if (BigDecimal.class.isAssignableFrom(type)) {
                arg = new BigDecimal(value.toString());
            }
            if (Enum.class.isAssignableFrom(type)) {
                // let's do it!
                for (Object enumConstant : type.getEnumConstants()) {
                    if (enumConstant.toString().equals(value)) {
                        arg = enumConstant;
                        break;
                    }
                }
            }
            return arg;
        }

        public static Object adapter(Annotation adapter, Object value) throws Exception {
            if (valueType(adapter).isAssignableFrom(Enum.class)) {
                for (Object enumConstant : valueType(adapter).getEnumConstants()) {
                    Object arg;
                    try {
                        arg = unmarshal(enumConstant, value);
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                    if (arg != null) {
                        return arg;
                    }
                }
            } else {
                return unmarshal(valueType(adapter).getDeclaredConstructor((Class<?>[]) null).newInstance((Object[]) null), value);
            }
            throw new RuntimeException("not support adapter");
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
    @interface XmlRootElement {
        String name();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
    @interface XmlAttribute {
        String name();

        String namespace() default "";

        boolean required() default true;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
    @interface XmlElement {
        String name();

        Class<?> type();
    }

    interface XmlAdapter {
        Object unmarshal(Object data);
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
    @interface XmlJavaTypeAdapter {
        Class<? extends XmlAdapter> value();
    }


    class SimpleXML implements XML {
        Type type = Type.Element;
        Map<String, String> attr = new LinkedHashMap<>();
        List<XML> elements = new ArrayList<>(8);
        String name;

        String path;

        String version;
        String encoding;

        @Override
        public String version() {
            return version == null ? XML.super.version() : version;
        }

        @Override
        public String encoding() {
            return encoding == null ? XML.super.encoding() : encoding;
        }

        @Override
        public Type type() {
            return type;
        }

        @Override
        public String name() {
            return name;
        }

        @Override
        public String path() {
            return path;
        }

        @Override
        public List<XML> content() {
            return elements;
        }

        @Override
        public Set<String> elementNameSet() {
            return elements.stream().map(XML::name)
                    .collect(Collectors.toSet());
        }

        @Override
        public Iterator<XML> elementByName(String name) {
            return content().stream()
                    .filter(xml -> xml.name().equals(name)).iterator();
        }

        @Override
        public Map<String, String> attributes() {
            return attr;
        }

        @Override
        public String toString() {
            return toString(false);
        }

        public String toString(boolean pretty) {
            XML lastElement = null;
            switch (type) {
                case String, Number, Boolean -> {
                    return name;
                }
                case Quote -> {
                    return "<!-- " + name + "--!>";
                }
                case Meta -> {
                    StringBuilder builder = new StringBuilder();
                    builder.append("<?");
                    builder.append(name);
                    attr.forEach((k, v) -> builder.append(" ").append(k).append("=\"").append(v).append("\""));
                    builder.append("?>");
                    return builder.toString();
                }
            }
            StringBuilder builder = new StringBuilder();
            if (path == null) {
                builder.append("<?xml version=\"").append(version()).append("\" encoding=\"").append(encoding()).append("\"?>");
                builder.append("\n");
            } else {
                // start insert the \t and \n
                if (pretty) {
                    builder.append("\n");
                    int tabCount = path.split("\\.").length;
                    builder.append("\t".repeat(tabCount));
                }
            }
            builder.append("<")
                    .append(name);
            if (!attr.isEmpty()) {
                // append the attr
                for (String key : attr.keySet()) {
                    builder.append(" ")
                            .append(key).append("=\"")
                            .append(attr.get(key))
                            .append("\"");
                }
            }
            if (content().isEmpty()) {
                builder.append("/>");
            } else {
                // append the content
                builder.append(">");
                for (XML xml : content()) {
                    builder.append(xml.toString(pretty));
                    lastElement = xml;
                }
                // start insert the \t and \n
                if (lastElement != null && lastElement.type() == Type.Element && pretty) {
                    builder.append("\n");
                    int tabCount = path == null ? 0 : path.split("\\.").length;
                    builder.append("\t".repeat(tabCount));
                }
                builder.append("</")
                        .append(name).append(">");
            }
            return builder.toString();
        }

        public void setRoot(String name) {
            path = name;
            for (XML element : elements) {
                if (element instanceof SimpleXML) {
                    ((SimpleXML) element).setRoot(name == null ? this.name : name + "." + this.name);
                }
            }
        }
    }

    static SimpleXML newline() {
        return ofString("\n");
    }

    static SimpleXML ofString(String str) {
        SimpleXML strXML = new SimpleXML();
        if (str.contains("<") || str.contains("&")) {
            str = "<![CDATA[" + str + "]]>";
        }
        strXML.name = str;
        strXML.type = Type.String;
        return strXML;
    }

    static SimpleXML meta(String name, Map<String, String> meta) {
        SimpleXML xmlMeta = new SimpleXML();
        xmlMeta.name = name;
        xmlMeta.attr = meta;
        xmlMeta.type = Type.Meta;
        return xmlMeta;
    }

    static SimpleXML of(String name, List<? extends XML> xmls) {
        return of(name, Collections.emptyMap(), xmls);
    }

    static SimpleXML of(String name, Map<String, String> attr, List<? extends XML> xmls) {
        SimpleXML simpleXML = new SimpleXML();
        simpleXML.name = name;
        simpleXML.attr = attr;
        for (XML xml : xmls) {
            if (xml instanceof SimpleXML) {
                ((SimpleXML) xml).setRoot(name);
            }
        }
        simpleXML.elements = new ArrayList<>(xmls);
        return simpleXML;
    }

    static XML parse(InputStream inputStream) throws SAXException, ParserConfigurationException, IOException {
        SAXParser saxParser = SAXParserFactory.newInstance()
                .newSAXParser();
        PathParser pathParser = new PathParser();
        saxParser.parse(inputStream, pathParser);
        return pathParser.getXML();
    }

    class PathParser extends DefaultHandler {
        SimpleXML xml = null;
        SimpleXML currentXml = null;
        ArrayDeque<SimpleXML> xmlArrayDeque = new ArrayDeque<>();

        public XML getXML() {
            xml.setRoot(null);
            xml.encoding = encoding;
            xml.version = version;
            return xml;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (currentXml != null) {
                xmlArrayDeque.push(currentXml);
            }
            currentXml = new SimpleXML();
            currentXml.name = qName;
            currentXml.attr = new LinkedHashMap<>();
            for (int i = 0; i < attributes.getLength(); i++) {
                currentXml.attr.put(attributes.getQName(i), attributes.getValue(i));
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            SimpleXML lastXml = currentXml;
            if (!xmlArrayDeque.isEmpty()) {
                currentXml = xmlArrayDeque.pop();
                currentXml.elements.add(lastXml);
            }
            xml = currentXml;
        }

        String version = "1.0";
        String encoding = "UTF-8";

        @Override
        public void declaration(String version, String encoding, String standalone) throws SAXException {
            this.version = version;
            this.encoding = encoding;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (length == 0) return;
            currentXml.elements.add(XML.ofString(new String(ch, start, length)));
        }
    }
}
